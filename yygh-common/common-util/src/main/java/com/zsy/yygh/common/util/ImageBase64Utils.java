package com.zsy.yygh.common.util;

import org.apache.commons.codec.binary.Base64;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

/**
 * @author zhangshuaiyin
 * @date 2021-07-19 14:59
 */
public class ImageBase64Utils {
    public static void main(String[] args) {
        // 待处理的图片
        String imageFile = "D:\\yygh_work\\biaobaiju.jpg";
        System.out.println(getImageString(imageFile));
    }

    public static String getImageString(String imageFile) {
        try (InputStream is = new FileInputStream(new File(imageFile));) {
            byte[] data;
            data = new byte[is.available()];
            is.read(data);
            return new String(Base64.encodeBase64(data));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }
}
