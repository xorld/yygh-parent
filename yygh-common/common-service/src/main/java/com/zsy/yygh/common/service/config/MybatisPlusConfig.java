package com.zsy.yygh.common.service.config;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDateTime;

/**
 * @author zhangshuaiyin
 * @date 2021-07-15 14:22
 */
@Slf4j
@Configuration
@MapperScan(basePackages = {"com.zsy.yygh.**.mapper"})
public class MybatisPlusConfig {

    /**
     * 新版分页插件，注意选择数据库
     *
     * @return MybatisPlusInterceptor
     */
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        interceptor.addInnerInterceptor(new PaginationInnerInterceptor(DbType.MYSQL));
        log.info("Mybatis Plus 分页插件配置完成...");
        return interceptor;
    }

    /**
     * 自动填充配置
     * @return MetaObjectHandler
     */
    @Bean
    public MetaObjectHandler metaObjectHandler() {
        return new MetaObjectHandler() {
            @Override
            public void insertFill(MetaObject metaObject) {
                log.info("start insert fill ....");
                this.fillStrategy(metaObject, "createTime", LocalDateTime.now());
                this.fillStrategy(metaObject, "updateTime", LocalDateTime.now());
            }

            @Override
            public void updateFill(MetaObject metaObject) {
                log.info("start update fill ....");
                this.fillStrategy(metaObject, "updateTime", LocalDateTime.now());
            }
        };
    }
}
