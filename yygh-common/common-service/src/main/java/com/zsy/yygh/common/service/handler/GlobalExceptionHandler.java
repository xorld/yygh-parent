package com.zsy.yygh.common.service.handler;

import com.zsy.yygh.common.result.Result;
import com.zsy.yygh.common.service.exception.YyghException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 全局异常处理类
 *
 * @author zhangshuaiyin
 * @date 2021-07-15 16:15
 */
@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(Exception.class)
    @ResponseBody
    public Result<?> error(Exception e) {
        e.printStackTrace();
        return Result.fail();
    }

    /**
     * 自定义异常处理方法
     *
     * @param e
     * @return
     */
    @ExceptionHandler(YyghException.class)
    @ResponseBody
    public Result<?> error(YyghException e) {
        return Result.build(e.getCode(), e.getMessage());
    }
}
