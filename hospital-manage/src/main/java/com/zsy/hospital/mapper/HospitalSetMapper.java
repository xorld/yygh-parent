package com.zsy.hospital.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zsy.hospital.model.HospitalSet;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface HospitalSetMapper extends BaseMapper<HospitalSet> {

}
