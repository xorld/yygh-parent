package com.zsy.yygh.service.oss.service;

import org.springframework.web.multipart.MultipartFile;

/**
 * @author: zhangshuaiyin
 * @date: 2021/7/26 20:39
 */
public interface FileService {
    /**
     * 上传文件到阿里云oss
     *
     * @param file
     * @return
     */
    String upload(MultipartFile file);
}
