package com.zsy.yygh.service.oss;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author: zhangshuaiyin
 * @date: 2021/7/26 20:37
 */
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
@EnableDiscoveryClient
@ComponentScan(basePackages = {"com.zsy.yygh"})
public class OssServiceApp {
    public static void main(String[] args) {
        SpringApplication.run(OssServiceApp.class, args);
    }
}
