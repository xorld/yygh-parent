package com.zsy.yygh.service.oss.controller;

import com.zsy.yygh.common.result.Result;
import com.zsy.yygh.service.oss.service.FileService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author: zhangshuaiyin
 * @date: 2021/7/26 20:42
 */
@RestController
@RequestMapping("/api/oss/file")
public class FileApiController {

    @Autowired
    private FileService fileService;

    @ApiOperation("上传文件到阿里云oss")
    @PostMapping("fileUpload")
    @ApiImplicitParam(name = "file", dataType = "MultipartFile", allowMultiple = true)
    public Result fileUpload(MultipartFile file) {
        //获取上传文件
        String url = fileService.upload(file);
        return Result.ok(url);
    }
}
