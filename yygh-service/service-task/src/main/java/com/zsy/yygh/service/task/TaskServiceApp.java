package com.zsy.yygh.service.task;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author: zhangshuaiyin
 * @date: 2021/7/31 15:02
 */
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
@EnableDiscoveryClient
public class TaskServiceApp {
    public static void main(String[] args) {
        SpringApplication.run(TaskServiceApp.class, args);
    }
}
