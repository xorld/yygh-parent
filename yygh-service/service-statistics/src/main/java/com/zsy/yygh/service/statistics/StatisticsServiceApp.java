package com.zsy.yygh.service.statistics;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author: zhangshuaiyin
 * @date: 2021/7/31 15:15
 */
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
@EnableDiscoveryClient
@EnableFeignClients
@ComponentScan(basePackages = {"com.zsy.yygh.clent.order.client"})
public class StatisticsServiceApp {
    public static void main(String[] args) {
        SpringApplication.run(StatisticsServiceApp.class, args);
    }
}
