package com.zsy.yygh.service.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zsy.yygh.model.order.PaymentInfo;

/**
 * @author zhangshuaiyin
 * @date 2021-07-31
 */
public interface PaymentMapper extends BaseMapper<PaymentInfo> {
}
