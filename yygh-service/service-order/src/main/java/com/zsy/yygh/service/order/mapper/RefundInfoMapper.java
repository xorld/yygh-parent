package com.zsy.yygh.service.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zsy.yygh.model.order.RefundInfo;

/**
 * @author zhangshuaiyin
 * @date 2021-07-31
 */
public interface RefundInfoMapper extends BaseMapper<RefundInfo> {
}
