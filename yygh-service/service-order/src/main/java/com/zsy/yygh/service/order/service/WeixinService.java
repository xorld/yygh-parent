package com.zsy.yygh.service.order.service;

import java.util.Map;

/**
 * @author zhangshuaiyin
 * @date 2021-07-31
 */
public interface WeixinService {

    /**
     * 生成微信支付二维码
     *
     * @param orderId
     * @return
     */
    Map createNative(Long orderId);

    /**
     * 调用微信接口实现支付状态查询
     *
     * @param orderId
     * @return
     */
    Map<String, String> queryPayStatus(Long orderId);

    /***
     * 退款
     */
    Boolean refund(Long orderId);

}
