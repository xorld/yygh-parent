package com.zsy.yygh.service.order;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author zhangshuaiyin
 * @date 2021-07-27 16:22
 */
@SpringBootApplication
@ComponentScan(basePackages = {"com.zsy.yygh"})
@EnableDiscoveryClient
@EnableFeignClients(basePackages = {"com.zsy.yygh"})
public class OrderServiceApp {
    public static void main(String[] args) {
        SpringApplication.run(OrderServiceApp.class, args);
    }
}
