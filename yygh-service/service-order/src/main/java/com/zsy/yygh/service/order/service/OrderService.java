package com.zsy.yygh.service.order.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zsy.yygh.model.order.OrderInfo;
import com.zsy.yygh.vo.order.OrderCountQueryVo;
import com.zsy.yygh.vo.order.OrderQueryVo;

import java.util.Map;

/**
 * @author zhangshuaiyin
 * @date 2021-07-28 13:58
 */
public interface OrderService extends IService<OrderInfo> {
    /**
     * 保存订单
     *
     * @param scheduleId
     * @param patientId
     * @return
     */
    Long saveOrder(String scheduleId, Long patientId);

    /**
     * 分页列表
     */
    IPage<OrderInfo> selectPage(Page<OrderInfo> pageParam, OrderQueryVo orderQueryVo);

    /**
     * 获取订单详情
     */
    OrderInfo getOrderInfo(String id);

    /**
     * 订单详情
     *
     * @param orderId
     * @return
     */
    Map<String, Object> show(Long orderId);

    /**
     * 取消订单
     *
     * @param orderId
     */
    Boolean cancelOrder(Long orderId);

    /**
     * 就诊提醒
     */
    void patientTips();

    /**
     * 订单统计
     */
    Map<String, Object> getCountMap(OrderCountQueryVo orderCountQueryVo);

}
