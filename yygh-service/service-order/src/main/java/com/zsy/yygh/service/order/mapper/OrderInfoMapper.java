package com.zsy.yygh.service.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zsy.yygh.model.order.OrderInfo;
import com.zsy.yygh.vo.order.OrderCountQueryVo;
import com.zsy.yygh.vo.order.OrderCountVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author zhangshuaiyin
 * @date 2021-07-28 13:58
 */
public interface OrderInfoMapper extends BaseMapper<OrderInfo> {
    List<OrderCountVo> selectOrderCount(@Param("vo") OrderCountQueryVo orderCountQueryVo);
}
