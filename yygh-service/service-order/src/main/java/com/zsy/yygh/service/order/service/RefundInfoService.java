package com.zsy.yygh.service.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zsy.yygh.model.order.PaymentInfo;
import com.zsy.yygh.model.order.RefundInfo;

/**
 * @author zhangshuaiyin
 * @date 2021-07-31
 */
public interface RefundInfoService extends IService<RefundInfo> {

    /**
     * 保存退款记录
     *
     * @param paymentInfo
     */
    RefundInfo saveRefundInfo(PaymentInfo paymentInfo);

}
