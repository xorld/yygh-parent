package com.zsy.yygh.service.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zsy.yygh.model.order.OrderInfo;
import com.zsy.yygh.model.order.PaymentInfo;

import java.util.Map;

/**
 * @author zhangshuaiyin
 * @date 2021-07-31
 */
public interface PaymentService extends IService<PaymentInfo> {

    /**
     * 向支付记录表添加信息
     *
     * @param order
     * @param status
     */
    void savePaymentInfo(OrderInfo order, Integer status);

    /**
     * 更新订单状态
     *
     * @param out_trade_no
     * @param resultMap
     */
    void paySuccess(String out_trade_no, Map<String, String> resultMap);

    /**
     * 获取支付记录
     *
     * @param orderId
     * @param paymentType
     * @return
     */
    PaymentInfo getPaymentInfo(Long orderId, Integer paymentType);

}
