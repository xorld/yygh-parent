package com.zsy.yygh.hospital.repository;

import com.zsy.yygh.model.hospital.Department;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * MongoDB Department 持久层
 *
 * @author zhangshuaiyin
 * @date 2021-07-19 15:20
 */
@Repository
public interface DepartmentRepository extends MongoRepository<Department, String> {

    /**
     * 根据医院编号和科室编号确定科室
     *
     * @param hoscode 医院编号
     * @param depcode 科室编号
     * @return 科室
     */
    Department getDepartmentByHoscodeAndDepcode(String hoscode, String depcode);
}
