package com.zsy.yygh.hospital.api;

import com.zsy.yygh.common.result.Result;
import com.zsy.yygh.common.result.ResultCodeEnum;
import com.zsy.yygh.common.service.exception.YyghException;
import com.zsy.yygh.common.service.helper.HttpRequestHelper;
import com.zsy.yygh.hospital.service.DepartmentService;
import com.zsy.yygh.hospital.service.HospitalService;
import com.zsy.yygh.hospital.service.HospitalSetService;
import com.zsy.yygh.hospital.service.ScheduleService;
import com.zsy.yygh.model.hospital.Department;
import com.zsy.yygh.model.hospital.Hospital;
import com.zsy.yygh.model.hospital.Schedule;
import com.zsy.yygh.vo.hospital.DepartmentQueryVO;
import com.zsy.yygh.vo.hospital.ScheduleQueryVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * 平台对外开发的接口都写在该Controller类
 *
 * @author zhangshuaiyin
 * @date 2021-07-19 14:18
 */
@Api(tags = "医院管理API接口")
@RestController
@RequestMapping("/api/hosp")
public class ApiController {

    private final HospitalService hospitalService;
    private final HospitalSetService hospitalSetService;
    private final DepartmentService departmentService;
    private final ScheduleService scheduleService;

    public ApiController(HospitalService hospitalService,
                         HospitalSetService hospitalSetService,
                         DepartmentService departmentService,
                         ScheduleService scheduleService) {
        this.hospitalService = hospitalService;
        this.hospitalSetService = hospitalSetService;
        this.departmentService = departmentService;
        this.scheduleService = scheduleService;
    }

    @ApiOperation(value = "上传医院")
    @PostMapping("saveHospital")
    public Result<?> saveHospital(HttpServletRequest request) {
        Map<String, Object> paramMap = HttpRequestHelper.switchMap(request.getParameterMap());
        // 必须参数校验 略
        String hoscode = (String) paramMap.get("hoscode");
        checkParam(hoscode);

        // 传输过程中“+”转换为了“ ”，因此我们要转换回来
        String logoDataString = (String) paramMap.get("logoData");
        if (!StringUtils.isEmpty(logoDataString)) {
            String logoData = logoDataString.replaceAll(" ", "+");
            paramMap.put("logoData", logoData);
        }

        // 签名校验
        checkSign(paramMap, hoscode);

        hospitalService.save(paramMap);
        return Result.ok();
    }

    @ApiOperation(value = "获取医院信息")
    @PostMapping("hospital/show")
    public Result<Hospital> hospital(HttpServletRequest request) {
        Map<String, Object> paramMap = HttpRequestHelper.switchMap(request.getParameterMap());
        // 必须参数校验 略
        String hoscode = (String) paramMap.get("hoscode");
        checkParam(hoscode);
        // 签名校验
        checkSign(paramMap, hoscode);

        return Result.ok(hospitalService.getByHoscode((String) paramMap.get("hoscode")));
    }

    @ApiOperation(value = "上传科室")
    @PostMapping("saveDepartment")
    public Result<?> saveDepartment(HttpServletRequest request) {
        Map<String, Object> paramMap = HttpRequestHelper.switchMap(request.getParameterMap());
        // 必须参数校验 略
        String hoscode = (String) paramMap.get("hoscode");
        checkParam(hoscode);
        // 签名校验
        checkSign(paramMap, hoscode);

        departmentService.save(paramMap);
        return Result.ok();
    }

    @ApiOperation(value = "获取科室分页列表")
    @PostMapping("department/list")
    public Result<Page<Department>> department(HttpServletRequest request) {
        Map<String, Object> paramMap = HttpRequestHelper.switchMap(request.getParameterMap());
        // 必须参数校验 略
        String hoscode = (String) paramMap.get("hoscode");
        // 非必填
        String depcode = (String) paramMap.get("depcode");
        int page = StringUtils.isEmpty((CharSequence) paramMap.get("page")) ? 1 : Integer.parseInt((String) paramMap.get("page"));
        int limit = StringUtils.isEmpty((CharSequence) paramMap.get("limit")) ? 10 : Integer.parseInt((String) paramMap.get("limit"));

        checkParam(hoscode);
        // 签名校验
        checkSign(paramMap, hoscode);

        DepartmentQueryVO departmentQueryVo = new DepartmentQueryVO();
        departmentQueryVo.setHoscode(hoscode);
        departmentQueryVo.setDepcode(depcode);
        Page<Department> pageModel = departmentService.selectPage(page, limit, departmentQueryVo);
        return Result.ok(pageModel);
    }

    @ApiOperation(value = "删除科室")
    @PostMapping("department/remove")
    public Result<?> removeDepartment(HttpServletRequest request) {
        Map<String, Object> paramMap = HttpRequestHelper.switchMap(request.getParameterMap());
        // 必须参数校验 略
        String hoscode = (String) paramMap.get("hoscode");
        // 必填
        String depcode = (String) paramMap.get("depcode");
        checkParam(hoscode);
        // 签名校验
        checkSign(paramMap, hoscode);

        departmentService.remove(hoscode, depcode);
        return Result.ok();
    }

    @ApiOperation(value = "上传排班")
    @PostMapping("saveSchedule")
    public Result<?> saveSchedule(HttpServletRequest request) {
        Map<String, Object> paramMap = HttpRequestHelper.switchMap(request.getParameterMap());
        // 必须参数校验 略
        String hoscode = (String) paramMap.get("hoscode");
        checkParam(hoscode);
        // 签名校验
        checkSign(paramMap, hoscode);

        scheduleService.save(paramMap);
        return Result.ok();
    }

    @ApiOperation(value = "获取排班分页列表")
    @PostMapping("schedule/list")
    public Result<Page<Schedule>> schedule(HttpServletRequest request) {
        Map<String, Object> paramMap = HttpRequestHelper.switchMap(request.getParameterMap());
        // 必须参数校验 略
        String hoscode = (String) paramMap.get("hoscode");
        // 非必填
        String depcode = (String) paramMap.get("depcode");
        int page = StringUtils.isEmpty((CharSequence) paramMap.get("page")) ? 1 : Integer.parseInt((String) paramMap.get("page"));
        int limit = StringUtils.isEmpty((CharSequence) paramMap.get("limit")) ? 10 : Integer.parseInt((String) paramMap.get("limit"));

        checkParam(hoscode);
        // 签名校验
        checkSign(paramMap, hoscode);

        ScheduleQueryVO scheduleQueryVo = new ScheduleQueryVO();
        scheduleQueryVo.setHoscode(hoscode);
        scheduleQueryVo.setDepcode(depcode);
        Page<Schedule> pageModel = scheduleService.selectPage(page, limit, scheduleQueryVo);
        return Result.ok(pageModel);
    }

    @ApiOperation(value = "删除科室")
    @PostMapping("schedule/remove")
    public Result<?> removeSchedule(HttpServletRequest request) {
        Map<String, Object> paramMap = HttpRequestHelper.switchMap(request.getParameterMap());
        // 必须参数校验 略
        String hoscode = (String) paramMap.get("hoscode");
        // 必填
        String hosScheduleId = (String) paramMap.get("hosScheduleId");
        checkParam(hoscode);
        // 签名校验
        checkSign(paramMap, hoscode);

        scheduleService.remove(hoscode, hosScheduleId);
        return Result.ok();
    }

    private void checkParam(String params) {
        if (StringUtils.isEmpty(params)) {
            throw new YyghException(ResultCodeEnum.PARAM_ERROR);
        }
    }

    private void checkSign(Map<String, Object> paramMap, String hoscode) {
        if (!HttpRequestHelper.isSignEquals(paramMap, hospitalSetService.getSignKey(hoscode))) {
            throw new YyghException(ResultCodeEnum.SIGN_ERROR);
        }
    }
}
