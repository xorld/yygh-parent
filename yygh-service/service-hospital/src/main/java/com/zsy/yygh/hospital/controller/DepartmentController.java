package com.zsy.yygh.hospital.controller;

import com.zsy.yygh.common.result.Result;
import com.zsy.yygh.hospital.service.DepartmentService;
import com.zsy.yygh.vo.hospital.DepartmentVO;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author zhangshuaiyin
 * @date 2021-07-20 15:55
 */
@RestController
@RequestMapping("/admin/hosp/department")
public class DepartmentController {

    private final DepartmentService departmentService;

    public DepartmentController(DepartmentService departmentService) {
        this.departmentService = departmentService;
    }

    @ApiOperation(value = "查询医院所有科室列表", notes = "根据医院编号，查询医院所有科室列表")
    @GetMapping("getDeptList/{hoscode}")
    public Result<List<DepartmentVO>> getDeptList(@PathVariable String hoscode) {
        List<DepartmentVO> list = departmentService.findDeptTree(hoscode);
        return Result.ok(list);
    }
}
