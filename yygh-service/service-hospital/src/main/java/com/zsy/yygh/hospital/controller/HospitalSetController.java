package com.zsy.yygh.hospital.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.zsy.yygh.common.result.Result;
import com.zsy.yygh.common.util.MD5;
import com.zsy.yygh.model.hospital.HospitalSet;
import com.zsy.yygh.hospital.service.HospitalSetService;
import com.zsy.yygh.vo.hospital.HospitalSetQueryVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.Random;

/**
 * 医院设置表 前端控制器
 *
 * @author zhangshuaiyin
 * @since 2021-07-15
 */
@Api(tags = "医院设置管理")
@RestController
@RequestMapping("/admin/hospital/hospitalSet")
public class HospitalSetController {
    private final HospitalSetService hospitalSetService;

    public HospitalSetController(HospitalSetService hospitalSetService) {
        this.hospitalSetService = hospitalSetService;
    }

    @ApiOperationSupport(order = 1)
    @ApiOperation(value = "获取所有医院设置", notes = "查询医院设置表所有信息")
    @GetMapping("findAll")
    public Result<List<HospitalSet>> findAllHospitalSet() {
        return Result.ok(hospitalSetService.list());
    }

    @ApiOperationSupport(order = 2)
    @ApiOperation(value = "逻辑删除医院设置", notes = "逻辑删除医院设置")
    @DeleteMapping("{id}")
    public Result<?> removeHospSet(@ApiParam("医院设置id") @PathVariable Long id) {
        return hospitalSetService.removeById(id) ? Result.ok() : Result.fail();
    }

    @ApiOperationSupport(order = 3)
    @ApiOperation(value = "条件查询带分页", notes = "条件查询带分页")
    @PostMapping("{current}/{limit}")
    public Result<Page<HospitalSet>> findPageHospSet(@ApiParam("页码") @PathVariable long current,
                                                     @ApiParam("每页记录数") @PathVariable long limit,
                                                     @RequestBody(required = false) HospitalSetQueryVO hospitalSetQueryVo) {
        // 创建page对象，传递当前页，每页记录数
        Page<HospitalSet> page = new Page<>(current, limit);
        // 构建条件
        QueryWrapper<HospitalSet> wrapper = new QueryWrapper<>();
        //医院名称 医院编号
        String hosname = hospitalSetQueryVo.getHosname();
        String hoscode = hospitalSetQueryVo.getHoscode();
        wrapper.like(StringUtils.isNotEmpty(hosname), "hosname", hosname);
        wrapper.eq(StringUtils.isNotEmpty(hoscode), "hoscode", hoscode);

        return Result.ok(hospitalSetService.page(page, wrapper));
    }

    @ApiOperationSupport(order = 4)
    @ApiOperation(value = "添加医院设置", notes = "添加医院设置")
    @PostMapping("")
    public Result<?> saveHospitalSet(@Valid @RequestBody HospitalSet hospitalSet) {
        // 设置状态 0 使用 1 不能使用
        hospitalSet.setStatus(0);
        // 签名秘钥, 当前时间毫秒值+1000以内随机数 的 MD5 值
        hospitalSet.setSignKey(MD5.encrypt(System.currentTimeMillis() + "" + new Random().nextInt(1000)));
        // 调用 service save()
        return hospitalSetService.save(hospitalSet) ? Result.ok() : Result.fail();
    }

    @ApiOperationSupport(order = 5)
    @ApiOperation(value = "根据id获取医院设置", notes = "根据id获取医院设置")
    @GetMapping("{id}")
    public Result<HospitalSet> getHospSet(@ApiParam("医院设置id") @PathVariable Long id) {
        return Result.ok(hospitalSetService.getById(id));
    }

    @ApiOperationSupport(order = 6)
    @ApiOperation(value = "修改医院设置", notes = "修改医院设置")
    @PutMapping("")
    public Result<?> updateHospitalSet(@RequestBody HospitalSet hospitalSet) {
        return hospitalSetService.updateById(hospitalSet) ? Result.ok() : Result.fail();
    }

    @ApiOperationSupport(order = 7)
    @ApiOperation(value = "批量删除医院设置", notes = "批量删除医院设置")
    @DeleteMapping("batch")
    public Result<?> batchRemoveHospitalSet(@ApiParam("医院设置id数组") @RequestBody List<Long> idList) {
        return hospitalSetService.removeByIds(idList) ? Result.ok() : Result.fail();
    }

    @ApiOperationSupport(order = 8)
    @ApiOperation(value = "医院设置锁定和解锁", notes = "医院设置锁定和解锁")
    @PutMapping("lock/{id}/{status}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "医院设置id"),
            @ApiImplicitParam(name = "status", value = "锁定状态 1-锁定 0-解锁")
    })
    public Result<?> lockHospitalSet(@PathVariable Long id, @PathVariable Integer status) {
        //根据id查询医院设置信息
        HospitalSet hospitalSet = hospitalSetService.getById(id);
        //设置状态
        hospitalSet.setStatus(status);
        //调用方法
        return hospitalSetService.updateById(hospitalSet) ? Result.ok() : Result.fail();
    }

    @ApiOperationSupport(order = 9)
    @ApiOperation(value = "发送签名秘钥", notes = "发送签名秘钥")
    @PutMapping("signKey/{id}")
    public Result<?> lockHospitalSet(@ApiParam("医院设置id") @PathVariable Long id) {
        HospitalSet hospitalSet = hospitalSetService.getById(id);
        String signKey = hospitalSet.getSignKey();
        String hoscode = hospitalSet.getHoscode();
        // TODO 发送短信
        return Result.ok();
    }
}

