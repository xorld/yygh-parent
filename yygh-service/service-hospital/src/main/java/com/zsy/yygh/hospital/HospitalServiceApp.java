package com.zsy.yygh.hospital;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author zhangshuaiyin
 * @date 2021-07-15 10:17
 */
@ComponentScan(basePackages = {"com.zsy.yygh"})
@EnableFeignClients(basePackages = "com.zsy.yygh")
@EnableDiscoveryClient
@SpringBootApplication
public class HospitalServiceApp {
    public static void main(String[] args) {
        SpringApplication.run(HospitalServiceApp.class, args);
    }
}
