package com.zsy.yygh.hospital.service;

import com.zsy.yygh.model.hospital.Hospital;
import com.zsy.yygh.vo.hospital.HospitalQueryVO;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Map;

/**
 * @author zhangshuaiyin
 * @date 2021-07-19 14:17
 */
public interface HospitalService {
    /**
     * 上传医院信息
     *
     * @param paramMap 参数使用Map，减少对象封装，有利于签名校验，后续会体验到
     */
    void save(Map<String, Object> paramMap);

    /**
     * 查询医院
     *
     * @param hoscode 医院编码
     * @return Hospital
     */
    Hospital getByHoscode(String hoscode);

    /**
     * 分页查询
     *
     * @param page            当前页码
     * @param limit           每页记录数
     * @param hospitalQueryVo 查询条件
     * @return
     */
    Page<Hospital> selectPage(Integer page, Integer limit, HospitalQueryVO hospitalQueryVo);

    /**
     * 更新上线状态
     */
    void updateStatus(String id, Integer status);

    /**
     * 医院详情
     *
     * @param id
     * @return
     */
    Map<String, Object> show(String id);

    /**
     * 根据医院编号获取医院名称接口
     * @param hoscode
     * @return
     */
    String getName(String hoscode);

    /**
     * 根据医院名称获取医院列表
     */
    List<Hospital> findByHosname(String hosname);

    /**
     * 医院预约挂号详情
     */
    Map<String, Object> item(String hoscode);
}
