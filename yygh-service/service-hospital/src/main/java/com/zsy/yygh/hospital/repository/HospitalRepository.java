package com.zsy.yygh.hospital.repository;

import com.zsy.yygh.model.hospital.Hospital;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * MongoDB 持久层
 *
 * @author zhangshuaiyin
 * @date 2021-07-19 14:16
 */
@Repository
public interface HospitalRepository extends MongoRepository<Hospital, String> {
    /**
     * 根据唯一索引医院编号查询
     *
     * @param hoscode 医院编号
     * @return Hospital
     */
    Hospital getHospitalByHoscode(String hoscode);

    List<Hospital> findHospitalByHosnameLike(String hosname);
}
