package com.zsy.yygh.hospital.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zsy.yygh.model.hospital.HospitalSet;
import com.zsy.yygh.vo.order.SignInfoVo;

/**
 * 医院设置表 服务类
 *
 * @author zhangshuaiyin
 * @since 2021-07-15
 */
public interface HospitalSetService extends IService<HospitalSet> {
    /**
     * 获取签名key
     *
     * @param hoscode 医院编码
     * @return 签名key
     */
    String getSignKey(String hoscode);

    /**
     * 获取医院签名信息
     *
     * @param hoscode
     * @return
     */
    SignInfoVo getSignInfoVo(String hoscode);
}
