package com.zsy.yygh.hospital.service;

import com.zsy.yygh.model.hospital.Department;
import com.zsy.yygh.vo.hospital.DepartmentQueryVO;
import com.zsy.yygh.vo.hospital.DepartmentVO;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Map;

/**
 * @author zhangshuaiyin
 * @date 2021-07-19 15:21
 */
public interface DepartmentService {
    /**
     * 上传科室信息
     *
     * @param paramMap
     */
    void save(Map<String, Object> paramMap);

    /**
     * 分页查询
     *
     * @param page              当前页码
     * @param limit             每页记录数
     * @param departmentQueryVo 查询条件
     * @return
     */
    Page<Department> selectPage(Integer page, Integer limit, DepartmentQueryVO departmentQueryVo);

    /**
     * 删除科室
     *
     * @param hoscode
     * @param depcode
     */
    void remove(String hoscode, String depcode);

    /**
     * 根据医院编号，查询医院所有科室列表
     *
     * @param hoscode
     * @return
     */
    List<DepartmentVO> findDeptTree(String hoscode);

    /**
     * 根据科室编号，和医院编号，查询科室名称
     *
     * @param hoscode
     * @param depcode
     * @return
     */
    String getDepName(String hoscode, String depcode);

    /**
     * 获取部门
     */
    Department getDepartment(String hoscode, String depcode);
}
