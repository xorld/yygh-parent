package com.zsy.yygh.hospital.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zsy.yygh.model.hospital.HospitalSet;

/**
 * 医院设置表 Mapper 接口
 *
 * @author zhangshuaiyin
 * @since 2021-07-15
 */
public interface HospitalSetMapper extends BaseMapper<HospitalSet> {

}
