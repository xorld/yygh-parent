package com.zsy.yygh.hospital.repository;

import com.zsy.yygh.model.hospital.Schedule;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * @author zhangshuaiyin
 * @date 2021-07-19 16:02
 */
@Repository
public interface ScheduleRepository extends MongoRepository<Schedule, String> {
    /**
     * 根据医院编码和排班id确定排班信息
     *
     * @param hoscode       医院编码
     * @param hosScheduleId 排班id
     * @return 排班信息
     */
    Schedule getScheduleByHoscodeAndHosScheduleId(String hoscode, String hosScheduleId);

    /**
     * 根据医院编号 、科室编号和工作日期，查询排班详细信息
     * @param hoscode 医院编号
     * @param depcode 科室编号
     * @param toDate 工作日期
     * @return 排班详细信息
     */
    List<Schedule> findScheduleByHoscodeAndDepcodeAndWorkDate(String hoscode, String depcode, Date toDate);
}
