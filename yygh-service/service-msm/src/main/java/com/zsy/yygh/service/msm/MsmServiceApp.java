package com.zsy.yygh.service.msm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author zhangshuaiyin
 * @date 2021-07-21 15:40
 */
@ComponentScan("com.zsy.yygh")
@EnableDiscoveryClient
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
public class MsmServiceApp {
    public static void main(String[] args) {
        SpringApplication.run(MsmServiceApp.class, args);
    }
}
