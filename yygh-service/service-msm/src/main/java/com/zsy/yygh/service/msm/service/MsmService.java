package com.zsy.yygh.service.msm.service;

import com.zsy.yygh.vo.msm.MsmVo;

/**
 * @author zhangshuaiyin
 * @date 2021-07-21 15:43
 */
public interface MsmService {

    /**
     * 发送手机验证码
     *
     * @param phone
     * @param code
     * @return
     */
    boolean send(String phone, String code);

    boolean send(MsmVo msmVo);
}
