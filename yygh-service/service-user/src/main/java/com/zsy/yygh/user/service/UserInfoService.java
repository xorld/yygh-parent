package com.zsy.yygh.user.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zsy.yygh.model.user.UserInfo;
import com.zsy.yygh.vo.user.LoginVO;
import com.zsy.yygh.vo.user.UserAuthVo;
import com.zsy.yygh.vo.user.UserInfoQueryVo;

import java.util.Map;

/**
 * @author zhangshuaiyin
 * @date 2021-07-21 15:06
 */
public interface UserInfoService extends IService<UserInfo> {
    /**
     * 会员登录
     *
     * @param loginVo
     * @return
     */
    Map<String, Object> login(LoginVO loginVo);

    /**
     * 根据微信openid获取用户信息
     *
     * @param openid
     * @return
     */
    UserInfo getByOpenid(String openid);

    /**
     * 用户认证
     *
     * @param userId
     * @param userAuthVo
     */
    void userAuth(Long userId, UserAuthVo userAuthVo);

    /**
     * 用户列表（条件查询带分页）
     *
     * @param pageParam
     * @param userInfoQueryVo
     * @return
     */
    IPage<UserInfo> selectPage(IPage<UserInfo> pageParam, UserInfoQueryVo userInfoQueryVo);

    /**
     * 用户锁定
     *
     * @param userId
     * @param status 0：锁定 1：正常
     */
    void lock(Long userId, Integer status);

    /**
     * 详情
     *
     * @param userId
     * @return
     */
    Map<String, Object> show(Long userId);

    /**
     * 认证审批
     *
     * @param userId
     * @param authStatus 2：通过 -1：不通过
     */
    void approval(Long userId, Integer authStatus);
}
