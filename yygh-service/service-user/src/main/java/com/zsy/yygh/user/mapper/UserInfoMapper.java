package com.zsy.yygh.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zsy.yygh.model.user.UserInfo;

/**
 * @author zhangshuaiyin
 * @date 2021-07-21 15:06
 */
public interface UserInfoMapper extends BaseMapper<UserInfo> {
}
