package com.zsy.yygh.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zsy.yygh.model.user.Patient;

/**
 * @author zhangshuaiyin
 * @date 2021-07-27 09:30
 */
public interface PatientMapper extends BaseMapper<Patient> {
}
