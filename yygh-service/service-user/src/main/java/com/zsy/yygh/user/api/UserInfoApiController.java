package com.zsy.yygh.user.api;

import com.zsy.yygh.common.result.Result;
import com.zsy.yygh.common.util.AuthContextHolder;
import com.zsy.yygh.common.util.IpUtils;
import com.zsy.yygh.model.user.UserInfo;
import com.zsy.yygh.user.service.UserInfoService;
import com.zsy.yygh.vo.user.LoginVO;
import com.zsy.yygh.vo.user.UserAuthVo;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * @author zhangshuaiyin
 * @date 2021-07-21 15:08
 */
@RestController
@RequestMapping("/api/user")
public class UserInfoApiController {

    private final UserInfoService userInfoService;

    public UserInfoApiController(UserInfoService userInfoService) {
        this.userInfoService = userInfoService;
    }

    @ApiOperation(value = "会员登录")
    @PostMapping("login")
    public Result<Map<String, Object>> login(@RequestBody LoginVO loginVo, HttpServletRequest request) {
        loginVo.setIp(IpUtils.getIpAddr(request));
        Map<String, Object> info = userInfoService.login(loginVo);
        return Result.ok(info);
    }

    @ApiOperation(value = "用户认证接口")
    @PostMapping("auth/userAuth")
    public Result<?> userAuth(@RequestBody UserAuthVo userAuthVo, HttpServletRequest request) {
        //传递两个参数，第一个参数用户id，第二个参数认证数据vo对象
        userInfoService.userAuth(AuthContextHolder.getUserId(request), userAuthVo);
        return Result.ok();
    }

    @ApiOperation(value = "获取用户id信息接口")
    @GetMapping("auth/getUserInfo")
    public Result<UserInfo> getUserInfo(HttpServletRequest request) {
        Long userId = AuthContextHolder.getUserId(request);
        UserInfo userInfo = userInfoService.getById(userId);
        return Result.ok(userInfo);
    }
}
