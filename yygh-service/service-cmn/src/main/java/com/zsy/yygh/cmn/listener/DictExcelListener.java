package com.zsy.yygh.cmn.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.zsy.yygh.cmn.mapper.DictMapper;
import com.zsy.yygh.model.cmn.Dict;
import com.zsy.yygh.vo.cmn.DictExcelVO;
import org.springframework.beans.BeanUtils;

/**
 * @author: zhangshuaiyin
 * @date: 2021/7/17 18:54
 */
public class DictExcelListener extends AnalysisEventListener<DictExcelVO> {

    private final DictMapper dictMapper;

    public DictExcelListener(DictMapper dictMapper) {
        this.dictMapper = dictMapper;
    }

    /**
     * 一行一行读取，从第二行
     * 读取表头重写 invokeHeadMap() 方法
     *
     * @param dictExcelVO
     * @param analysisContext
     */
    @Override
    public void invoke(DictExcelVO dictExcelVO, AnalysisContext analysisContext) {
        // 调用方法添加数据库
        Dict dict = new Dict();
        BeanUtils.copyProperties(dictExcelVO, dict);
        dictMapper.insert(dict);
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {

    }
}
