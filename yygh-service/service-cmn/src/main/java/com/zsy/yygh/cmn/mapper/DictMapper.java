package com.zsy.yygh.cmn.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zsy.yygh.model.cmn.Dict;

/**
 * @author: zhangshuaiyin
 * @date: 2021/7/17 17:43
 */
public interface DictMapper extends BaseMapper<Dict> {
}
