package com.zsy.yygh.cmn.service;

import com.alibaba.excel.EasyExcel;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zsy.yygh.model.cmn.Dict;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @author: zhangshuaiyin
 * @date: 2021/7/17 17:44
 */
public interface DictService extends IService<Dict> {
    /**
     * 根据数据id查询子数据列表
     *
     * @param id
     * @return
     */
    List<Dict> findChildData(Long id);

    /**
     * 导出数据字典到 Excel
     *
     * @param response
     */
    void exportData(HttpServletResponse response);

    /**
     * 从 Excel 导入数据字典
     *
     * @param file
     */
    void importData(MultipartFile file);

    /**
     * 根据上级编码与值获取数据字典名称
     *
     * @param parentDictCode 上级编码
     * @param value          值
     * @return 数据字典
     */
    String getNameByParentDictCodeAndValue(String parentDictCode, String value);

    /**
     * 根据编码获取数据字典列表
     *
     * @param dictCode
     * @return
     */
    List<Dict> findByDictCode(String dictCode);
}
