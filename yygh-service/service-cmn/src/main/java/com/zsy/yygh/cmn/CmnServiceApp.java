package com.zsy.yygh.cmn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author: zhangshuaiyin
 * @date: 2021/7/17 17:40
 */
@ComponentScan(basePackages = {"com.zsy.yygh"})
@SpringBootApplication
public class CmnServiceApp {
    public static void main(String[] args) {
        SpringApplication.run(CmnServiceApp.class, args);
    }
}
