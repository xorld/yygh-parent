package com.zsy.yygh.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author zhangshuaiyin
 * @date 2021-07-21 09:41
 */
@SpringBootApplication
public class YyghGatewayApp {
    public static void main(String[] args) {
        SpringApplication.run(YyghGatewayApp.class, args);
    }
}
