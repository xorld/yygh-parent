package com.zsy.yygh.vo.hospital;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 医院设置查询 VO
 *
 * @author zhangshuaiyin
 * @since 2021-07-15
 */
@ApiModel(description = "医院设置查询VO")
@Data
public class HospitalSetQueryVO {

    @ApiModelProperty(value = "医院名称")
    private String hosname;

    @ApiModelProperty(value = "医院编号")
    private String hoscode;
}
