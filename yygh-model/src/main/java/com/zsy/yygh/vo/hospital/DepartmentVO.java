package com.zsy.yygh.vo.hospital;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author zhangshuaiyin
 * @date 2021-07-20 15:42
 */
@Data
@ApiModel(description = "Department")
public class DepartmentVO {

    @ApiModelProperty(value = "科室编号")
    private String depcode;

    @ApiModelProperty(value = "科室名称")
    private String depname;

    @ApiModelProperty(value = "下级节点")
    private List<DepartmentVO> children;

}
