package com.zsy.yygh.model.hospital;

import com.zsy.yygh.model.base.BaseMongoEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Department
 *
 * @author zhangshuaiyin
 * @date 2021-07-19 15:17
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(description = "Department")
@Document("Department")
public class Department extends BaseMongoEntity {

    private static final long serialVersionUID = 1L;

    @Indexed //普通索引
    @ApiModelProperty(value = "医院编号")
    private String hoscode;

    @Indexed(unique = true) //唯一索引
    @ApiModelProperty(value = "科室编号")
    private String depcode;

    @ApiModelProperty(value = "科室名称")
    private String depname;

    @ApiModelProperty(value = "科室描述")
    private String intro;

    @ApiModelProperty(value = "大科室编号")
    private String bigcode;

    @ApiModelProperty(value = "大科室名称")
    private String bigname;
}
