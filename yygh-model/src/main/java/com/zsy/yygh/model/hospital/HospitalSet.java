package com.zsy.yygh.model.hospital;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zsy.yygh.model.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;

/**
 * HospitalSet 医院设置表
 *
 * @author zhangshuaiyin
 * @since 2021-07-15
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(description = "医院设置")
@TableName("hospital_set")
public class HospitalSet extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "医院名称")
    @TableField("hosname")
    @NotBlank
    private String hosname;

    @ApiModelProperty(value = "医院编号")
    @TableField("hoscode")
    @NotBlank
    private String hoscode;

    @ApiModelProperty(value = "api基础路径")
    @TableField("api_url")
    private String apiUrl;

    @ApiModelProperty(value = "签名秘钥")
    @TableField("sign_key")
    private String signKey;

    @ApiModelProperty(value = "联系人姓名")
    @TableField("contacts_name")
    @NotBlank
    private String contactsName;

    @ApiModelProperty(value = "联系人手机")
    @TableField("contacts_phone")
    @NotBlank
    private String contactsPhone;

    @ApiModelProperty(value = "状态 1-解锁 0-锁定")
    @TableField("status")
    private Integer status;

}

