# 尚医通-线上医院预约挂号系统

个人笔记：https://www.yuque.com/zhangshuaiyin/yygh-parent

尚医通即为网上预约挂号系统，网上预约挂号是近年来开展的一项便民就医服务，旨在缓解看病难 挂号难的就医难题，许多患者为看一次病要跑很多次医院，最终还不一定能保证看得上医生。网上预约挂号全面提供的预约挂号业务从根本上解决了这一就医难题。随时随地轻松挂号！不用排长队！

# 技术选择

## 后端

| 技术选型                                                     | 版本     | 描述                                                        |
| ------------------------------------------------------------ | -------- | ----------------------------------------------------------- |
| [SpringBoot](https://spring.io/projects/spring-boot)         | 2.4.2    | Java 体系内实现微服务最佳方案                               |
| [SpringCloud](https://spring.io/projects/spring-cloud)       | 2020.0.0 | 基于Spring Boot实现的云原生应用开发框架                     |
| [SpringCloud Alibaba](https://spring.io/projects/spring-cloud-alibaba) | 2021.1   | Alibaba 发布的分布式微服务解决方案                          |
| [Mybatis Plus](https://mp.baomidou.com/)                     | 3.4.2    | 持久层框架，简化持久层开发流程                              |
| MySQL                                                        | 8.0.29   | 关系型数据库                                                |
| [Swagger/Knife4j](https://doc.xiaominfo.com/)                | 3.0.2    | knife4j是为Java MVC框架集成Swagger生成Api文档的增强解决方案 |

NOSQL 数据库: MongoDB Redis 

Windows bin 目录下常用命令启动：

**双击启动 windows bat 脚本启动 redis MongoDB nacos**

脚本启动参考：https://www.yuque.com/zhangshuaiyin/yygh-parent/azu3ug

```bash
# redis
redis-server redis.windows.conf

# nginx 双击启动
nginx.exe

# mongoDB
mongod --config mongod.cfg

# nacos
startup.cmd -m standalone
```

## 前端

| 技术选型                                  | 版本 | 描述                                                         |
| ----------------------------------------- | ---- | ------------------------------------------------------------ |
| [Vue.js](https://cn.vuejs.org/)           | 2.x  | 渐进式 JavaScript 框架                                       |
| [Element UI](https://element.eleme.cn/#/) | 2.x  | Element，一套为开发者 设计师和产品经理准备的基于 Vue 2.0 的桌面端组件库 |
| [Node.js](http://nodejs.cn/)              |      | 基于 Chrome V8 引擎的 JavaScript 运行环境                    |
| [Axios](http://www.axios-js.com/)         |      | 基于 promise 易用 简洁且高效的http库                         |
| [Babel](https://www.babeljs.cn/)          |      | Babel 是一个工具链，主要用于将采用 ECMAScript 2015+ 语法编写的代码转换为向后兼容的 JavaScript 语法，以便能够运行在当前和旧版本的浏览器或其他环境中。 |
| [Webpack](https://www.webpackjs.com/)     |      | 前端打包工具                                                 |



# 项目架构

![项目架构图](https://cdn.nlark.com/yuque/0/2021/png/12568777/1626354327428-87f37aae-3169-461f-9d4a-3e2e186d1c8a.png)

# 业务流程

![业务流程图](https://cdn.nlark.com/yuque/0/2021/png/12568777/1626354337819-baf30a17-7104-47c7-8fb8-08dd3a7affa9.png)



# 发行版代码更新日志：

## 1. v0.0.1-工程搭建 医院设置后端接口实现

代码地址：https://gitee.com/zsy0216/yygh-parent/tree/v0.0.1/

该版本对应到视频 p24

1. 项目基本架构搭建
   
2. Mybatis Plus 持久层相关配置

3. Swagger Knife4j api 文档配置

4. 医院设置增删改查接口实现相关代码

5. 统一返回结果 统一异常处理 统一日志处理


## 2. v0.0.2-医院设置前端实现

代码地址：https://gitee.com/zsy0216/yygh-parent/tree/v0.0.2/

该版本对应到视频 p56

1. 前端工程搭建

2. 医院设置相关页面开发及后端接口联调

## 3. v0.0.3-数据字典实现

代码地址：https://gitee.com/zsy0216/yygh-parent/tree/v0.0.3/

该版本对应到视频 p65

1. 数据字典后端接口实现
   
2. 数据字典前端页面实现

3. EasyExcel 导入导出

4. 数据字典添加 redis 缓存

5. 前端 Nginx 配置

## 4. v0.0.4-数据接口实现

代码地址：https://gitee.com/zsy0216/yygh-parent/tree/v0.0.4/

对应git 发行版本 v0.0.4

该版本对应到视频 p80

1. 医院模拟系统整合联调

2. 上传医院 医院查询 修改 删除接口实现；

3. 上传科室 科室查询 修改 删除接口实现；

4. 上传排版 排版查询 修改 删除接口实现；

## 5. v0.0.5-医院管理实现

该阶段对应git 发行版 v0.0.5-5.医院管理实现

https://gitee.com/zsy0216/yygh-parent/tree/v0.0.5/

对应视频前 p98 所有内容

1. 整合 Nacos OpenFeign

2. 医院列表-上线状态 详情
   
3. 排班管理-科室 排班日期显示

## 6. v0.0.6-尚医通前台用户系统

该阶段对应git 发行版 v0.0.6尚医通前台用户系统

https://gitee.com/zsy0216/yygh-parent/tree/v0.0.6/

对应视频前 p144 所有内容

1. 使用 NUXT 服务端渲染技术

2. 平台首页搭建

3. 平台首页-医院详情 预约挂号 预约须知

4. 用户服务登录接口实现(手机短信)

## 7. master-尚医通项目所有代码

master 分支对应视频所有内容，包含 v0.0.6 及其之后所有代码。

1. 微信登录

2. 阿里云OSS 用户认证与就诊人

3. 预约挂号

4. 预约下单

5. 微信支付

6. 定时任务与统计

# 项目总结

## 1. 后台管理系统

### 1.1 医院设置管理

- 医院设置列表 添加 锁定 删除
- 医院列表 详情 排班 下线

### 1.2 数据管理

- 数据字典树形显示 导入 导出

### 1.3 用户管理

- 用户列表 查看 锁定
- 认证用户审批

### 1.4 订单管理

- 订单列表 详情

### 1.5 统计管理

- 预约统计

## 2. 前台用户系统

### 2.1 首页数据显示  

- 医院列表    

### 2.2 医院详情显示

- 医院科室显示    

###  2.3 用户登录功能

- 手机号登录（短信验证码发送）  
- 微信扫描登录   

###  2.4 用户实名认证    

###  2.5 就诊人管理  

- 列表 添加 详情 删除  

###  2.6 预约挂号功能  

- 排班和挂号详情信息  
- 确认挂号信息  
- 生成预约挂号订单  
- 挂号订单支付（微信）  
- 取消预约订单  

###   2.7 就医提醒功能  

## 3. 后端技术总结

### 3.1 SpringBoot 

### 3.2 SpringCloud 

- Nacos注册中心  
- Feign  
- GateWay   

### 3.3 Redis

- 使用Redis作为缓存  
- 验证码有效时间 支付二维码有效时间    

### 3.4 MongoDB  

- 使用MongoDB存储 医院相关数据     

### 3.5 EasyExcel  

- 操作excel表格，进行读和写操作     

### 3.6 MyBatisPlus     

### 3.7 RabbitMQ  

- 订单相关操作，发送mq消息    

### 3.8 Docker  

- 下载镜像 docker pull   

（2）创建容器 docker run     

### 3.9 阿里云OSS

### 3.10 阿里云短信服务

### 3.11 微信登录/支付

### 3.12 定时任务

## 4. 前端技术总结

### 4.1 vue 

- 指令     

### 4.2 Element-ui     

### 4.3 nuxt     

### 4.4 npm     

### 4.5 ECharts
